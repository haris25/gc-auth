# README

To run the project:

1. Get Google Service Account credentials (see https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/keys/create)

2. Save the credentials as json file, e.g. `credentials.json`.

3. To get the content of credentials.json as environment variable, run

   ```
   GCP_SERVICE_ACCOUNT_CONTENTS=$(cat credentials.json )
   ```

4. To setup ansible, run:

   ```
   virtualenv .venv -p /usr/bin/python3
   source .venv/bin/activate
   pip install -r requirements.txt
   ```

5. Execute ansible:

   ```
   ansible-playbook -i localhost sites.yml -v
   ```

   